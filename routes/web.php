<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');

// Admin Add Items
Route::get('/additem', 'ItemController@create');

// To Save Item
Route::post('/additem', 'ItemController@store');

// To Delete Item
Route::delete('/deleteitem/{id}', 'ItemController@destroy');

// To Edit Item
Route::get('/edititem/{id}', 'ItemController@edit');

// To Save Item
Route::patch('/edititem/{id}', 'ItemController@update');

// Cart CRUD
Route::post('/addtocart/{id}', 'ItemController@addToCart');

Route::get('/cart', 'ItemController@showCart');

Route::delete('/removeitem/{id}', 'ItemController@removeItem');

Route::delete('/emptycart/', 'ItemController@emptyCart');

Route::get('/checkout', 'OrderController@checkout');

// Orders
Route::get('/showorders', 'OrderController@showOrders');